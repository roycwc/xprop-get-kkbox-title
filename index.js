const exec = require('child_process').execSync
const filterListStr = '_NET_CLIENT_LIST(WINDOW): window id # '
const filterWindowStr = 'kkbox-web-player-nativefier-'
const filterTitleStr = '_NET_WM_NAME(UTF8_STRING) = '

let stdout = exec('xprop -root').toString()
let result = stdout.split('\n').filter((it)=>{
  return 0 === it.indexOf(filterListStr)
}).map(it=>{
  return it.substr(filterListStr.length)
}).join('').split(', ').map(it=>{
   return exec(`xprop -id ${it}`).toString()
}).filter(it=>{
  return it.indexOf(filterWindowStr) >= 0
}).join('').split('\n').filter(it=>{
  return it.indexOf(filterTitleStr) >= 0
}).map(it=>{
  return it.substr(filterTitleStr.length).replace(/\"/g,'')
}).join('')

console.log(result)
