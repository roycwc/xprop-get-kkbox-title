# Show KKBOX webapp music title
Using node.js to query xprop window info, get current kkbox 
music title. 
Used for i3-block to display current music in KKBOX webapp

# Pre-requisis
- node.js v9+

# Usage
```sh
node .

// "欲言又止 - TVB劇集<溏心風暴3>片尾曲 - 菊梓喬 (HANA)"
```

<img src="https://lh3.googleusercontent.com/d9OCYSlnFfOgIjdqPmwgOrz_Lcnt0bxXGXLPOfiHqdrIUu8UJ90ERRdBYcdbghuulOn5J9AQbimwQkoMN6bvm_dbBylZHyQVldCRyx4G_Fwv4FQuTNq4XsQ3tj4cOvpwQ0Tdd3zaU7Q0n-4ceHqMOEJkuObCF7NWOYV4rFqBTZYqK_qYkpPhVr_d8AeRgeV3L5h_FTtClyHleys_NS6A5Yy9l_LKEbxezCj71GjNepdVHILL15e1Rcm9Ch5c3aGMKu1yJyk6KbFGLrmrd758IDYSnnf10FRal6-1O-W1rVgHGM4X4UIWzU87HrMMQ5eArRRenkbiTdNh1MWkMeqxiP9mM2ntr_ikgFR0qBuypjlh2CS19M6He0dKJFslxy7Dt-TO-lmpwtWs3ih7nbGeCozrSbX4spDDsdIgmkFah0tqRNqzkqITDI-cwwzUTk1fTyM21WN4d5CUJk8C1cQwDYu2DFU73tbYIlhC3OD__t4vwfEnfK_EEFT7g_-Wrc7uuIFEI_1h-AiMvMVjhHhJ6RoR4TaoVJha9h4nBHG0ow3JUajDspkyled5qdf0FSHyouXLLwFVZkDAoQJf0whIAZtNUhw5q_Axm3j-sd0=w718-h404-no" />
